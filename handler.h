#pragma once
/*
 * tcp net handler for client.
 */
#include <functional>
#include <map>
#include <vector>
#include "singleton.h"
#include "rpc/rpc_handle.hpp"
#include "proto.h"
#include "redis_client.h"

class CNetHandler;
class CCliSession;
using handleFunc = std::function<void(CCliSession* pCliSession, const char* proto, int len)>;

// TCP net handler.
class CNetHandler final {
	DECLARE_SINGLETON(CNetHandler);
protected:
	CNetHandler() = default;
	virtual ~CNetHandler() = default;

public:
	redisSpace::RedisClient* getRedis() {
		return &m_redisClient;
	}

public:
	// Init initialize the TCP handler.
	void Init();

	// HandleMsg handle message.
	bool HandleMsg(CCliSession* pCliSession, const char* msg, int len);

protected:
	// handlePing handle ping.
	void handlePing(CCliSession* pCliSession, const char* proto, int len);

private:
	// message id to its handler.
	std::map<unsigned short, handleFunc> m_handlers;

	// redis client.
	redisSpace::RedisClient m_redisClient;
};