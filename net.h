#pragma once
/*
 * tcp server listener.
 */
#include "all.hpp"

class CListener final {
public:
	CListener();
	virtual ~CListener();

public:
	bool Init(const char* addr, unsigned int threadSize);

private:
	anet::tcp::Server *m_server;
	anet::tcp::ICodec* m_codec;
	anet::tcp::ISessionFactory* m_factory;
};