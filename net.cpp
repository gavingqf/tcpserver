#include "net.h"
#include "share_ptr_session.hpp"
#include "default_interface.hpp"
#include "session.h"

IMPLEMENT_SINGLETON(CCliSessionFactory);
CListener::CListener() : m_server(nullptr),
    m_codec(nullptr),
    m_factory(nullptr) {
}

CListener::~CListener() {
	if (m_server != nullptr) {
		delete m_server;
		m_server = nullptr;
	}
	
	if (m_codec != nullptr) {
		delete m_codec;
		m_codec = nullptr;
	}
}

bool CListener::Init(const char *addr, unsigned int threadSize) {
	m_server = new anet::tcp::Server(threadSize);
	m_codec = new CBigCodec();
	m_factory = CCliSessionFactory::Instance()->GetFactory();

	auto& server = *m_server;
	server->setPacketParser(m_codec);
	server->setSessionFactory(m_factory);
	return server->start(addr);
}
