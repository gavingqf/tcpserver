#pragma once
/*
 * now time instance.
 */

#include "singleton.h"

class CNowTime {
protected:
    DECLARE_SINGLETON(CNowTime);
    CNowTime() : m_ts(0) {}
    ~CNowTime() {}

public:
    void SetNowTime(long long ts) {
        m_ts = ts;
    }
    long long GetNowTime() {
        return m_ts;
    }

private:
    long long m_ts;
};
