#include "handler.h"
#include "session.h"
#include <utility>
#include "server.h"

// bind function.
#define bindFunc(memberFunc) std::bind(&memberFunc, this, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3)

IMPLEMENT_SINGLETON(CNetHandler);
void CNetHandler::Init() {
	m_handlers[0] = bindFunc(CNetHandler::handlePing);
	// add more register here.
}

bool CNetHandler::HandleMsg(CCliSession* pCliSession, const char* msg, int len) {
	const char* protoMsg = msg + anet::tcp::gProto_head_size;
	unsigned short msgId = ntohs(*(unsigned short*)protoMsg);
	const char* proto = protoMsg + anet::tcp::gProto_message_id_size;

	// set beat heart time.
	pCliSession->SetBeatHeartTime(GetNowMSTime());

	auto it = m_handlers.find(msgId);
	if (it != m_handlers.end()) {
		// handle in local server.
		auto protoLen = len - anet::tcp::gProto_head_size - anet::tcp::gProto_message_id_size;
		it->second(pCliSession, proto, protoLen);
		return true;
	} else {
		// call game server's function.
		auto pGameServer = CServerMgr::Instance()->findGameServer();
		if (pGameServer != nullptr) {
			return pGameServer->remote_call("CGateServer::clientHandler", pCliSession->GetId(), std::string(msg, len));
		} else {
			return false;
		}
	}
}

void CNetHandler::handlePing(CCliSession* pCliSession, const char* proto, int len) {
	pCliSession->Send(0, "pong", int(strlen("pong")));
}
