#pragma once
/*
 * detail server handler: Handle() for message and OnStatus() for status change.
 */
#include <map>
#include "singleton.h"
#include "rpc/rpc_handle.hpp"
#include "proto.h"
#include "pipe_module.h"

class CGameServerHandler;
class CDBServerHandler;
using rpcGameHandler = anet::rpc_codec::handle_service<CGameServerHandler, CServer>;
using rpcDBHandler = anet::rpc_codec::handle_service<CDBServerHandler, CServer>;

// db server handle.
class CDBServerHandler : public rpcDBHandler, public IServerHandler {
public:
	DECLARE_SINGLETON(CDBServerHandler);
protected:
	CDBServerHandler();
	virtual ~CDBServerHandler() = default;

public:
	virtual void Handle(const char* msg, int len, CServer* pServer);
	virtual void OnStatus(bool isConnect, CServer* pServer);

protected:
	void rpcTestRes(CServer* pServer, int a, std::string& c, std::vector<int>& v, Example& n);

private:
	CServer* m_pServer;
};

// game server handle.
class CGameServerHandler : public rpcGameHandler, public IServerHandler {
	DECLARE_SINGLETON(CGameServerHandler);
protected:
	CGameServerHandler();
	virtual ~CGameServerHandler() = default;

public:
	virtual void Handle(const char* msg, int len, CServer* pServer);
	virtual void OnStatus(bool isConnect, CServer* pServer);

protected:
	void testRPCCall(CServer* pServer, int a, std::string& b, std::vector<int>& c, Example& e);

	// game to client call: client message handler.
	void clientHandler(CServer* pServer, unsigned int id, const std::string& protoMsg);

private:
	CServer* m_pServer;
};

// add more server.