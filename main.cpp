#include <stdlib.h>
#include "framework.h"
#include "log/log.h"

/*
 * main module.  
 */
int main(int argc, char* argv[]) {
	try {
		CFrameWork framework;
		const char* path;
		if (argc > 1) {
			path = argv[1];
		} else {
			path = ".";
		}

		framework.CreateInstance();
		if (framework.Init(path)) {
			framework.Run(1024);
		}
		framework.Release();
		return 0;
	} catch (const std::exception& e) {
		LogCrit("Error: %s", e.what());
		return -1;
	} catch (...) {
		LogCrit("Unknown error occurred");
		return -1;
	}
}
