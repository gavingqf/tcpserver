#include "config.h"
#include "util/config.h"
#include "log.h"
#include <iostream>

bool CConfig::Load(const char* filePath) {
	anet::utils::CConfig reader;
	if (!reader.Parse(std::string(filePath) + "/gate.conf")) {
		LogCrit("load %s error:%s", filePath, reader.GetLastError().c_str());
		return false;
	}

	// net thread size.
	m_threadSize = reader.GetNode<unsigned int>("main/listen/thread_size", 0);

	// ip.
	m_ip = reader.GetNode<std::string>("main/listen/ip", "0");
	
	// port
	if (!reader.GetNodeWithResult<unsigned short>("main/listen/port", m_port)) {
		LogCrit("get main/port error:%s", reader.GetLastError().c_str());
		return false;
	}

	// redis information.
	m_RedisIP = reader.GetNode<std::string>("main/redis/redis_ip", "");
	if (!reader.GetNodeWithResult<unsigned short>("main/redis/redis_port", m_RedisPort)) {
		LogCrit("get main/redis_port error:%s", reader.GetLastError().c_str());
		return false;
	}
	m_RedisPasswd = reader.GetNode<std::string>("main/redis/redis_password", "");

	return true;
}
