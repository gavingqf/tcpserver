# summary 
  这个是一个gateserver的模板。它支持和客户端的消息id通讯，也支持和服务器直接的rpc调用。这个进程依赖我的anet, pipe和service_discovery等相关的模块。

# 配置说明
  1. 日志配置:log.conf,主要是配置日志输出的配置信息。
  ```xml
  <log>
     # 日志等级, 0:debug, 1:info, 2 critical
     log_level = 1
     # 日志路径
     log_path = ./log
     # 日志输出频率，单位：ms
     log_interval = 1000
     # 日志名词
     log_name = gate
  </log>
  ```
  
  2. 基本的配置：gate.conf, 主要配置gateserver的服务器启动相关信息。
  ```xml
  <main>
      # thread size.
      thread_size = 3

      # server address：ip:port.
      ip = 0
      port = 5566
 </main>
 ```

  3. 服务器互联配置：服务发现以及互联节点信息配置, pipe.xml
  ```xml
  <?xml version='1.0' encoding='utf-8'?>
<!-- pipe config -->
<pipe_config>
    <!-- node information -->
    <!-- the name and id must be unique in the group -->
    <!-- you can set the listenip to empty to not listen on it -->
    <!-- the name can be find in the connection_relation node-->
    <node name="game" id="1" listenip="127.0.0.1" listenport="23456"/>

    <!-- service discovery information(now using redis) -->
    <service_discovery address="127.0.0.1:6379" index="0" password="" key="my_service_discovery@126.com"/>

    <!-- node's connection relations -->
    <connection_relation>         
        <relation node="game"        connect="" key="game"/>
        <relation node="db"          connect="game, center" key="db"/>
        <relation node="gate"        connect="game, name,center,db" key="gate"/>
        <relation node="login"       connect="center,logincenter" key="login"/>
        <relation node="center"      connect=" " key="center"/>
        <relation node="name"        connect="" key="name"/>
        <relation node="logincenter" connect="" key="logincenter"/>
        <relation node="room"        connect="game" key="room"/>
    </connection_relation>
</pipe_config>
```

# 运行命令
./gateserver ./ &
