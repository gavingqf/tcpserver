#include "framework.h"
#include "main_service.h"
#include "timer/time_wheel.h"
#include "util/config.h"
#include "handler.h"
#include "session.h"
#include "pipe_module.h"
#include "server.h"
#include "ts.h"
#include "util/time.hpp"
#include "log/log.h"

void CFrameWork::CreateInstance() {
	CNetHandler::CreateInstance();
	CServerMgr::CreateInstance();
	CPipeModule::CreateInstance();
	CNowTime::CreateInstance();
	CServerMgr::CreateInstance();
	CDBServerHandler::CreateInstance();
	CGameServerHandler::CreateInstance();
	CCliSessionFactory::CreateInstance();
}

bool CFrameWork::Init(const char* path) {
	// initialize log module.
	if (!this->initLog(path)) {
		return false;
	}

	// load config.
	if (!m_conf.Load(path)) {
		return false;
	}

	// initialize tcp net handler.
	CNetHandler::Instance()->Init();

	// initialize pipe module.
	auto pipePath = std::string(path) + "/pipe.xml";
	if (!CPipeModule::Instance()->Init(pipePath.c_str(), CServerMgr::instance())) {
		LogCrit("initialize pipe with %s path error", pipePath.c_str());
		return false;
	}
	LogInfo("initialize %s pipe module ok.", pipePath.c_str());

	// start listening for client.
	char addr[1024] = { 0 };
	std::snprintf(addr, sizeof(addr) - 1, "%s:%d", m_conf.m_ip.c_str(), m_conf.m_port);
	if (!m_listener.Init(addr, m_conf.m_threadSize)) {
		LogCrit("initialize listener on %s error", addr);
		return false;
	}
	LogInfo("initialize tcp listener on %s ok.", addr);

	// initialize redis module.
	/*
	try {
		CNetHandler::Instance()->getRedis()->Init(m_conf.m_RedisIP, m_conf.m_RedisPort, m_conf.m_RedisPasswd);
	} catch (...) {
		LogCrit("initialize redis on %s:%d error", m_conf.m_RedisIP, m_conf.m_RedisPort);
		return false;
	}*/

	// log out.
	LogInfo("server is initialized ok.");

	return true;
}

void CFrameWork::Run(int count) {
	const int MIN_SLEEP_US = 1000;
	const int TIMEOUT_VALUE = 20;
	long long lastTime = GetNowMSTime();
	for (; !this->isSignaled();) {
		auto loopStart = GetNowMSTime();
		CNowTime::instance()->SetNowTime(loopStart);
		
        // deltaTime.
        long long deltaTime = loopStart - lastTime;

		// Timer update
		STimeWheelSpace::CTimeWheel::instance().update(deltaTime);

		// Pipe update.
		bool didWork = CPipeModule::Instance()->Run(static_cast<int>(deltaTime));

		// Sleep for a while
		if (!didWork) {
			std::this_thread::yield();
			std::this_thread::sleep_for(std::chrono::microseconds(MIN_SLEEP_US));
		}

		// Save last time.
		lastTime = loopStart;

		// Detail log for time out.
		if (deltaTime >= TIMEOUT_VALUE) {
			LogAInfo("Run time out:%d", deltaTime);
		}
	}
}

void CFrameWork::Release() {
	LogCrit("server is exit.");
}

bool CFrameWork::initLog(const char *path) {
	anet::utils::CConfig reader;
	if (!reader.Parse((std::string(path) + "/log.conf").c_str())) {
		std::cerr << "load " << "./log.conf" << " error:" << reader.GetLastError() << std::endl;
		return false;
	}

	// log config information.
	auto logLevel = reader.GetNode<int>("log/log_level", 0);
	auto logPath = reader.GetNode<std::string>("log/log_path", "./log");
	auto logInterval = reader.GetNode<int>("log/log_interval", 1000);
	auto logName = reader.GetNode<std::string>("log/log_name", "");

	// initialize the log module
	auto ret = anet::log::initLog(logPath, logName, anet::log::eLogLevel(logLevel), logInterval);
	if (!ret) {
		std::cerr << "initialize log error" << std::endl;
	}
	return ret;
}
