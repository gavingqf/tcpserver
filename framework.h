#pragma once
/*
 * framework.
 */
#include "config.h"
#include "net.h"
#include "signals.h"

class CFrameWork : public anet::utils::CSignal {
public:
	CFrameWork() = default;
	virtual ~CFrameWork() = default;

public:
	void CreateInstance();
	bool Init(const char *path);
	void Run(int count);
	void Release();

protected:
	// initialize log module.
	bool initLog(const char *path);

private:
	// framework config.
	CConfig m_conf;

	// tcp listener.
	CListener m_listener;
};