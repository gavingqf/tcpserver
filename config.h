#pragma once
/*
 * config module. 
 */
#include <string>

class CConfig final {
public:
	CConfig() = default;
	virtual ~CConfig() = default;

public:
	bool Load(const char* filePath);

public:
	// tcp listening information.
	unsigned int m_threadSize;
	std::string  m_ip;
	unsigned short m_port;

	// redis connection information.
	std::string m_RedisIP;
	unsigned short m_RedisPort;
	std::string m_RedisPasswd;
};