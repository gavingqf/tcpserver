#pragma once
/*
 * protocol struct for msgpack.  
 */

 // sub struct.
struct subStruct {
	int a;
	int b;
	std::vector<int> c;
	MSGPACK_DEFINE(a, b, c);
};

// test struct.
struct Example {
	int x;
	std::string y;
	int a[16];
	subStruct c;
	MSGPACK_DEFINE(x, y, a, c);
};